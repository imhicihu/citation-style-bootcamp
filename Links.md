## Links
* [Citation styles Developers branch](https://citationstyles.org/developers/#/language-specification)
* [Find and edit CSL citation styles](http://editor.citationstyles.org/about/)
* [Islandora Scholar](https://github.com/Islandora/islandora_scholar)

## Related links
* [Citationsy](https://citationsy.com/)
* [The Chicago Manual of Style Online](https://www.chicagomanualofstyle.org/home.html)

## To check
* [CiteData](https://github.com/seboettg/CiteData)