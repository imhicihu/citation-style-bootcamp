## Technical requirements ##

* Hardware
      - Macbook 15"
      - Macbook 13"
      - an internet connection (stable: a must condition)
* Software
     - [Citation style plugin for OJS](https://github.com/pkp/citationStyleLanguage)
     - [Pandoc](https://github.com/jgm/pandoc/): The universal markup converter
     - [Carbon](https://carbon.now.sh/): (Automatization on code screen sharing)
     - Bibliographic searcher:
        - [ScienceFair](http://sciencefair-app.com): Discover, collect, organise, read and analyse scientific papers.

* Online tools
     - [Citation styles editor -online-](http://editor.citationstyles.org/codeEditor/)
     - [Citation styles](https://citationstyles.org/)
     - [Github Citation styles](https://github.com/citation-style-language)
     - [Citation styles Test suite](https://github.com/citation-style-language/test-suite)
     - [Data converters](http://okfnlabs.org/dataconverters/): Unified python library and command line interface to convert data from one format to another (especially tabular data).
     - [Smash](https://www.fromsmash.com/): unlimited moving/transfer of digital assets
     - [Plantuml](http://www.plantuml.com/plantuml/uml/):  Diagram / deployment diagram / critical path
     - Data validator
         - [Goodtables](https://goodtables.io/): frictionless tool to validate data, particularly spreadsheets
     
* Documentation
     - [Developer certificate](https://developercertificate.org/)
     
## Legal ##

* All trademarks are the property of their respective owners.