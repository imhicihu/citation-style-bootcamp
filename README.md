![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)


# RATIONALE #

* This project is build along one purpose: automatization of citation styles for our periodic publications.
* This repo is a living document that will grow and adapt over time

### What is this repository for? ###

* Quick summary
    - Automate our citation styles for our periodic journals
* Version 1.01

### Colophon ###

* Check our current [colophon](https://bitbucket.org/imhicihu/citation-style-bootcamp/src/master/Colophon.md)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/citation-style-bootcamp/issues). _Up to date_, it�s a private issue tracker.

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/citation-style-bootcamp/commits/) section for the current status

### Related repositories ###

* Some repositories linked with this project:
     - [Temas Medievales project](https://bitbucket.org/imhicihu/temas-medievales-project/src/master/)
     - [Scopus Metadata](https://bitbucket.org/imhicihu/scopus-metadata/src/master/)

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/citation-style-bootcamp/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/citation-style-bootcamp/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)